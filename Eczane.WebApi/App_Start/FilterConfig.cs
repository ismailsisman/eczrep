﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eczane.BL.Manage; 


namespace Eczane.WebApi.App_Start
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new AuthorizeAttribute());
            filters.Add(new AuthorizeAttribute());
            filters.Add(new Auth());
            //filters.Add(new Auth1());
            //filters.Add(new RestrictIPsAttribute());
        }
    }
}