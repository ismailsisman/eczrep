﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml.XPath;
using Eczane.BL.Manage;

namespace Eczane.WebApi.Controllers
{
    [RoutePrefix("api/Admin")]
    public class AdminController : ApiController
    {
        private readonly EczaneDbContext ctx;
        public AdminController()
        {
            ctx= new EczaneDbContext();
        }

        /// <summary>
        /// Istekleri Goruntule
        /// </summary>
        /// <returns></returns>
        [Route("İstekleriGörüntüle")]
        [HttpGet]
        public IHttpActionResult IstekleriGoruntule()
        {
            try
            {
                var result = ctx.IstekBilgileri.Include("KalfaEczaci").Include("Eczaci").ToList();
                foreach (var item in result)
                {
                    item.Eczaci.IstekBilgileri = null;
                    item.KalfaEczaci.IstekBilgileri = null;
                }
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        /// <summary>
        /// id ya bağlı isteği Onayla
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("İstekgiOnayla/{id:int}")]
        [HttpPost]
        public IHttpActionResult IstekleriOnayla(int id)
        {
            try
            {
                var result = ctx.IstekBilgileri.FirstOrDefault(x=> x.Id==id);
                result.AdminIstekOnayTarihi=DateTime.Now;
                result.IstekGonderildiMi = true;
                ctx.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
