﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Http;
using Eczane.BL.Entities;
using Eczane.BL.Helpers;
using Eczane.BL.Manage;
using Eczane.WebApi.Models;
using Microsoft.AspNet.Identity;

namespace Eczane.WebApi.Controllers
{
    [System.Web.Http.RoutePrefix("api")]
    public class KisiselBilgilerController : ApiController
    {
        private readonly EczaneDbContext ctx;
        public KisiselBilgilerController()
        {
            ctx= new EczaneDbContext();
        }

        [Route("KisiBilgisileri")]
        [HttpGet]
        public IHttpActionResult GetKisiBilgileri()
        {
            try
            {
                var result = ctx.KisiBilgiler.ToList();
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [Route("KisiBilgisileri/{id:int}")]
        [HttpGet]
        public IHttpActionResult GetKisiBilgileri(int id)
        {
            try
            {
                var result = ctx.KisiBilgiler.FirstOrDefault(x => x.Id == id);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        /// <summary>
        /// User id ne göre KisiselBilgileri Kayıt Eder
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("User/{id:int}/Kaydet/KisiselBilgileri")]
        public IHttpActionResult Kaydet([FromUri]int id,[FromBody]KisiselBilgilerDTO dto)
        {
            try
            {
                string dosyaUrl = "";
                if (dto.Foto != null)
                {
                    dosyaUrl = FileUploadHelper.LoadFile(dto.Foto.DosyaBase64, Path.GetExtension(dto.Foto.DosyaAdi), dto.Foto.DosyaAdi.Substring(0, dto.Foto.DosyaAdi.LastIndexOf(".", StringComparison.Ordinal)), Convert.ToInt32(User.Identity.GetUserId()), true, null, true);
                }
                var ent=new LoginEnt.KisiBilgiler()
                {
                    Ad = dto.Ad,
                    SoyAd = dto.SoyAd,
                    Adres = dto.Adres,
                    IlId = dto.IlId,
                    IlceId = dto.IlceId,
                    Telefon = dto.Telefon,
                    FotoUrl = dosyaUrl
                };
                var result = ctx.KisiBilgiler.Add(ent);
                ctx.SaveChanges();
                var user = ctx.User.FirstOrDefault(x => x.Id == id);
                user.KisiId = result.Id;
                ctx.SaveChanges();
                return Ok("Kayıt Başarılı");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        /// <summary>
        /// User id ne göre KisiselBilgileri gunceller
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("User/{id:int}/Kaydet/KisiselBilgileri")]
        public IHttpActionResult Guncelle([FromUri]int id, [FromBody]KisiselBilgilerDTO dto)
        {
            try
            {
                string dosyaUrl = "";
                if (dto.Foto != null)
                {
                    string pathToFiles = HttpContext.Current.Server.MapPath("/");
                    var newpath = pathToFiles.Substring(0, pathToFiles.IndexOf("EczaneWeb") + 9) + "\\" + "Eczane.WebUI\\" ;
                    var eskiFoto = ctx.User.Include("Kisi").FirstOrDefault(x => x.Id == id).Kisi.FotoUrl;
                    if (eskiFoto != null)
                    {
                        var dosyaYolu = ctx.Dosya.FirstOrDefault(c => c.DosyaUrl == eskiFoto).DosyaUrl.Remove(0, 1);
                        if (System.IO.File.Exists(@newpath + dosyaYolu.Replace("/", "\\")))
                        {
                            System.IO.File.Delete(@newpath + dosyaYolu.Replace("/", "\\"));
                        }
                    }
                    dosyaUrl = FileUploadHelper.LoadFile(dto.Foto.DosyaBase64, Path.GetExtension(dto.Foto.DosyaAdi), dto.Foto.DosyaAdi.Substring(0, dto.Foto.DosyaAdi.LastIndexOf(".", StringComparison.Ordinal)), Convert.ToInt32(User.Identity.GetUserId()), true, null, true);
                }          
                var result = ctx.User.Include("Kisi").FirstOrDefault(x=> x.Id==id).Kisi;
                var ent = new LoginEnt.KisiBilgiler()
                {
                    Ad = dto.Ad,
                    SoyAd = dto.SoyAd,
                    Adres = dto.Adres,
                    IlId = dto.IlId,
                    IlceId = dto.IlceId,
                    Telefon = dto.Telefon,
                    FotoUrl = dosyaUrl,
                    Id = result.Id
                };
                ctx.Entry(result).CurrentValues.SetValues(ent);
                ctx.SaveChanges();
                return Ok("Kayıt Başarılı");
            }
            catch (Exception e)
            {
                    return BadRequest(e.Message);
            }
        }
    }
}
