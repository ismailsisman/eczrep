﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Eczane.BL.Entities;
using Eczane.BL.Manage;
using Eczane.WebApi.Models;
using Eczane.WebApi.Providers;
using Eczane.WebApi.Results;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
namespace Eczane.WebApi.Controllers
{
    //[System.Web.Http.Authorize]
    [System.Web.Http.RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private UserManager _userManager;
        private EczaneDbContext ctx;
        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }
        public AccountController()
        {
            ctx = new EczaneDbContext();
        }
        public AccountController(UserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
           
        }
        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        [System.Web.Http.Route("LoginInfo")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetUserInfo()
        {

            var user = HttpContext.Current.User;
            var id = Convert.ToInt32(user.Identity.GetUserId());
            if (ctx.User.Include("Kisi").ToList().Any(x=> (x.Kisi==null && x.Id==id)))
            {
                return Ok("Bu Kullanicı için Kisi Bilgisi Yok");
            }
            //var UserBilgisi=ctx.User.Where(x=> x.Id==id).FirstOrDefault();
            var ecz = ctx.Eczaci.FirstOrDefault(x => x.UserId == id);
            var kalfecz = ctx.KalfaEczaci.FirstOrDefault(x => x.UserId == id);
            if (ecz != null)
            {
                LoginInfoDTO<LoginEnt.Eczaci> Info = new LoginInfoDTO<LoginEnt.Eczaci>();
                Info.UserBilgiler = ecz;
                ecz.User.Kisi = null;
                ecz.User.Password = null;
                ecz.User.PasswordHash = null;
                ecz.User.SecurityStamp = null;
                return Ok(Info);
            }         
           else if (kalfecz != null)
            {
                LoginInfoDTO<LoginEnt.KalfaEczaci> Info = new LoginInfoDTO<LoginEnt.KalfaEczaci>();
                Info.UserBilgiler = kalfecz;
                kalfecz.User.Kisi = null;
                kalfecz.User.Password = null;
                kalfecz.User.PasswordHash = null;
                kalfecz.User.SecurityStamp = null;
                return Ok(Info);
            }
            else
            {
                return Ok(ctx.User.FirstOrDefault(x=> x.Id==id));
            }
            //Info.KisiselBilgiler = UserBilgisi.Kisi;           
        }
        [System.Web.Http.OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [System.Web.Http.Route("RegisterExternal")]
        public async Task<IHttpActionResult> RegisterExternal(RegisterBindingModel model)
        {
            if (model.Password!=model.ConfirmPassword)
            {
                return Ok("Şifreler Aynı Değil");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
          
            var user = new LoginEnt.User() { UserName = model.Email, Email = model.Email,Password =model.Password};
            IdentityResult result = await UserManager.CreateAsync(user);           
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            var userId = ctx.User.First(x => x.Email == model.Email).Id;
            if (model.EczaciMiKalfaMi == 1)
            {
                var roleId = ctx.Role.FirstOrDefault(x => x.Id == 2).Id;
                var eczaci = new LoginEnt.Eczaci()
                {
                    UserId = userId,
                    GIN_Numarasi = model.GINNo
                };
                ctx.Eczaci.Add(eczaci);
                ctx.SaveChanges();
                ctx.UserRole.Add(new LoginEnt.UserRole()
                {
                    RoleId = roleId,
                    UserId = eczaci.UserId.Value
                });
                ctx.SaveChanges();
            }
            else
            {
                var roleId = ctx.Role.FirstOrDefault(x => x.Id == 3).Id;
                var kalfa = new LoginEnt.KalfaEczaci()
                {
                    UserId = userId
                };
                ctx.KalfaEczaci.Add(kalfa);
                ctx.SaveChanges();
                var role = new LoginEnt.UserRole()
                {
                    RoleId = roleId,
                    UserId = kalfa.UserId.Value
                };
                ctx.UserRole.Add(role);
                ctx.SaveChanges();
            }
            Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            ClaimsIdentity oAuthIdentity = await CreateUserIdentityClass.GenerateUserIdentityAsync(user,
                OAuthDefaults.AuthenticationType);
            ClaimsIdentity cookieIdentity = await CreateUserIdentityClass.GenerateUserIdentityAsync(user,
                CookieAuthenticationDefaults.AuthenticationType);

            AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user.UserName);
            Authentication.SignIn(properties, oAuthIdentity, cookieIdentity);
            //var info = await Authentication.GetExternalLoginInfoAsync();
            //var a = HttpContext.Current.GetOwinContext().Authentication;
            //if (info == null)
            //{
            //    return InternalServerError();
            //}
            //result = await UserManager.AddLoginAsync(user.Id, info.Login);
            //if (!result.Succeeded)
            //{
            //    return GetErrorResult(result);
            //}
            return Ok();
        }
        [System.Web.Http.OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [System.Web.Http.AllowAnonymous]
        [System.Web.Http.Route("ExternalLogin", Name = "ExternalLogin")]
        public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        {
            if (error != null)
            {
                return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
            }
            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                return new ChallengeResult(provider, this);
            }

            LoginEnt.User user = await UserManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider,
                externalLogin.ProviderKey));

            bool hasRegistered = user != null;

            if (hasRegistered)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                ClaimsIdentity oAuthIdentity = await CreateUserIdentityClass.GenerateUserIdentityAsync(user,
                    OAuthDefaults.AuthenticationType);
                ClaimsIdentity cookieIdentity = await CreateUserIdentityClass.GenerateUserIdentityAsync(user,
                    CookieAuthenticationDefaults.AuthenticationType);

                AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user.UserName);
                Authentication.SignIn(properties, oAuthIdentity, cookieIdentity);
            }
            else
            {
                IEnumerable<Claim> claims = externalLogin.GetClaims();
                ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
                Authentication.SignIn(identity);
            }

            return Ok();
        }
        #region Helpers
        private IAuthenticationManager Authentication
        {
            get { return HttpContext.Current.GetOwinContext().Authentication; }
        }
        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }
        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }
        #endregion
    }
}