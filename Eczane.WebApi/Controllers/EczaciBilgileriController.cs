﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using Eczane.BL.Entities;
using Eczane.BL.Manage;
namespace Eczane.WebApi.Controllers
{
   // [AuthApi]
    [System.Web.Http.RoutePrefix("api")]
    public class EczaciBilgileriController : ApiController
    {
        public EczaneDbContext ctx;
        /// <summary>
        /// Eczaci Api controller
        /// </summary>
        public EczaciBilgileriController()
        {
            ctx = new EczaneDbContext();
        }
        /// <summary>
        /// Eczaların Hepsini getirir
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.Authorize]
        [System.Web.Http.Route("Eczacilar")]
        [System.Web.Http.HttpGet]
        [ResponseType(typeof(List<LoginEnt.Eczaci>))]
        public IHttpActionResult GetAll()
        {
            return Ok(ctx.Eczaci.Include("Kisi").ToList());
        }
        /// <summary>
        /// id ye göre Eczacı bilgisi getirir
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [System.Web.Http.Route("Eczacilar/{id:int}")]
        [System.Web.Http.HttpGet]
        [ResponseType(typeof(LoginEnt.Eczaci))]
        public IHttpActionResult Get(int id)
        {            
            return Ok(ctx.Eczaci.Include("Kisi").FirstOrDefault(x=> x.Id==id));
        }
        /// <summary>
        /// Eczacı Bilgisi Ekle
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [System.Web.Http.Route("Eczacilar")]
        [System.Web.Http.HttpPost]
        [ResponseType(typeof(LoginEnt.Eczaci))]
        public IHttpActionResult Add(LoginEnt.Eczaci dto)
        {
            if (!ModelState.IsValid)
            {
                BadRequest();
            }
            LoginEnt.Eczaci result = new LoginEnt.Eczaci();         
            try
            {
                using (var ctx = new EczaneDbContext())
                {
                    result = ctx.Eczaci.Add(dto);
                    ctx.SaveChanges();
                }
                return Ok(StatusCode(HttpStatusCode.OK));
            }
            catch (Exception e)
            {
                return Ok(e.Message);
            }
        }
        /// <summary>
        /// Eczacı Bilgisini gunceller
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        [System.Web.Http.Route("Eczacilar/{id:int}")]
        [System.Web.Http.HttpPut]
        [ResponseType(typeof(LoginEnt.Eczaci))]
        public IHttpActionResult Update(int id, [FromBody] LoginEnt.Eczaci dto)
        {
            if (!ModelState.IsValid)
            {
                BadRequest(ModelState);
            }
            try
            {
                var data = ctx.Eczaci.Find(id);
                ctx.Entry(data).CurrentValues.SetValues(dto);
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Ok(new HttpStatusCodeResult(HttpStatusCode.ExpectationFailed,e.Message));
            }
        }
        /// <summary>
        /// İd ye göre Eczacı Bilgisini siler
        /// </summary>
        /// <param name="id"></param>
        /// <param name=""></param>
        /// <returns></returns>
        [System.Web.Http.Route("Eczacilar/{id:int}")]
        [System.Web.Http.HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var data = ctx.Eczaci.Find(id);
            ctx.Eczaci.Remove(data);
            try
            {
                ctx.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Ok(new HttpStatusCodeResult(HttpStatusCode.ExpectationFailed, e.Message));
            }
        }
        /// <summary>
        /// id ye göre KisiBilgisini Getirir
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [System.Web.Http.Route("Eczacilar/{id:int}/KisiBilgisi")]
        [System.Web.Http.HttpGet]
        [ResponseType(typeof(List<LoginEnt.KisiBilgiler>))]
        public IHttpActionResult EczaciKisiBilgisi(int id)
        {
            try
            {
                var result = ctx.Eczaci.Include("Kisi").FirstOrDefault(x=> x.Id==id).Kisi;
                return Ok(result);
            }
            catch (Exception e)
            {
                return Ok(new HttpStatusCodeResult(HttpStatusCode.ExpectationFailed, e.Message));
            }
        }
        /// <summary>
        /// id li eczaci kalfaid li kalfaya istekde bulunur
        /// </summary>
        /// <param name="id"></param>
        /// <param name="kalfaId"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("Eczaci/{id:int}/KalfaEczaci/{kalfaId:int}")]
        public IHttpActionResult KalfaEczaciIstek(int id,int kalfaId)
        {
            try
            {
                var ecz = ctx.Eczaci.FirstOrDefault(x => x.Id == id);
                var kalf = ctx.KalfaEczaci.FirstOrDefault(x => x.Id == kalfaId);
                var istek = new LoginEnt.IstekBilgileri()
                {
                    EczId = ecz.Id,
                    KalfaId = kalf.Id,
                    AdminIstekOnayTarihi = null,
                    IstekGonderilmeTarihi = DateTime.Now
                };
                ctx.IstekBilgileri.Add(istek);
                ctx.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                return Ok(new HttpStatusCodeResult(HttpStatusCode.ExpectationFailed, e.Message));
            }
        }
        /// <summary>
        /// Database dispose
        /// </summary>
        protected void Dispose()
        {
            ctx.Dispose();
        }
    }
}
