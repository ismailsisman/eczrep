﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using Eczane.BL.Entities;
using Eczane.BL.Helpers;
using Eczane.BL.Manage;
using Eczane.WebApi.Models;
using Microsoft.AspNet.Identity;
namespace Eczane.WebApi.Controllers
{
   // [AuthApi]
    [System.Web.Http.RoutePrefix("api")]
    public class KalfaEczaciBilgileriController : ApiController
    {
        public EczaneDbContext ctx;
        /// <summary>
        /// Eczaci Api controller
        /// </summary>
        public KalfaEczaciBilgileriController()
        {
            ctx = new EczaneDbContext();
        }
        /// <summary>
        /// Eczaların Hepsini getirir
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.Route("KalfaEczacilar")]
        [System.Web.Http.HttpGet]
        [ResponseType(typeof(List<LoginEnt.KalfaEczaci>))]
        public IHttpActionResult GetAll()
        {
            var result = ctx.KalfaEczaci.Include("Kisi").ToList().Select(x=> new KalfaDTO()
            {
                User = null,
                Kisi = x.Kisi,
                Dosyalar = ctx.Dosya.Where(p=> p.UserId==x.UserId).ToList(),
                MezunOlduguFakulteAdi = x.MezunOlduguFakulteAdi,
                MezunOlduguTarih = x.MezunOlduguTarih,
                SistemPuan = x.SistemPuan,
                Id = x.Id
            }).ToList();
            return Ok(result);
        }
        [System.Web.Http.Route("KalfaEczacilar/Search")]
        [System.Web.Http.HttpGet]
        [ResponseType(typeof(List<LoginEnt.KalfaEczaci>))]
        public IHttpActionResult GetAllSeacrh(string term)
        {
            var nameList = term.Split(' ');
            return Ok(ctx.KalfaEczaci.Include("Kisi").Where(x=> nameList.All(p=> x.Kisi.Ad.ToLower().Contains(p) ||
            x.Kisi.SoyAd.ToLower().Contains(p))
            ).ToList());
        }
        /// <summary>
        /// id ye göre KalfaEczaci bilgisi getirir
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [System.Web.Http.Route("KalfaEczacilar/{id:int}")]
        [System.Web.Http.HttpGet]
        [ResponseType(typeof(LoginEnt.KalfaEczaci))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var KalfaEcazi = ctx.KalfaEczaci.FirstOrDefault(x => x.Id == id);
                var User = ctx.User.Include("Dosyalar").Include("Kisi").FirstOrDefault(x => x.Id == KalfaEcazi.UserId);
                KalfaDTO result = new KalfaDTO()
                {
                    User = null,
                    Kisi = KalfaEcazi.Kisi,
                    Dosyalar = User.Dosyalar,
                    MezunOlduguFakulteAdi = KalfaEcazi.MezunOlduguFakulteAdi,
                    MezunOlduguTarih = KalfaEcazi.MezunOlduguTarih,
                    SistemPuan = KalfaEcazi.SistemPuan,
                    Id = KalfaEcazi.Id
                };
                foreach (var item in result.Dosyalar)
                {
                    item.User = null;
                }
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }         
        }
        /// <summary>
        /// KalfaEczaci Bilgisi Ekle
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [System.Web.Http.Route("KalfaEczacilar")]
        [System.Web.Http.HttpPost]
        [ResponseType(typeof(LoginEnt.KalfaEczaci))]
        public IHttpActionResult Add(KalfaDTO dto)
        {
            string dosyaUrl="";
            var UserId = Convert.ToInt32(User.Identity.GetUserId());
            if (!ModelState.IsValid)
            {
                BadRequest();
            }
            LoginEnt.KalfaEczaci result = new LoginEnt.KalfaEczaci();
            using (var ctx = new EczaneDbContext())
            {
                if (ctx.KalfaEczaci.Any(x => x.UserId == UserId))
                    return BadRequest("Bu kullanicya ait Kalfa Bilgisi var.");
                LoginEnt.KalfaEczaci ent= new LoginEnt.KalfaEczaci()
                {
                    KisiId = dto.KisiId,
                    MezunOlduguFakulteAdi = dto.MezunOlduguFakulteAdi,
                    MezunOlduguTarih = dto.MezunOlduguTarih,
                    UserId = dto.UserId==null || dto.UserId==0?UserId:dto.UserId,
                    SistemPuan = dto.SistemPuan
                };
                if (dto.Dosya!=null || dto.DosyaBase64!=null || dto.DosyaBase64!="")
                {
                    dosyaUrl = dosyaUrl = FileUploadHelper.LoadFile(dto.DosyaBase64, Path.GetExtension(dto.Dosya.DosyaAdi), dto.Dosya.DosyaAdi.Substring(0, dto.Dosya.DosyaAdi.LastIndexOf(".", StringComparison.Ordinal)), Convert.ToInt32(User.Identity.GetUserId()), true, null, false);
                }
            }
            try
            {
                using (var ctx = new EczaneDbContext())
                {
                    ctx.SaveChanges();
                }
                return Ok(StatusCode(HttpStatusCode.OK));
            }
            catch (Exception e)
            {
                return Ok(e.Message);
            }
        }
        /// <summary>
        /// KalfaEczaci Bilgisini gunceller
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        [System.Web.Http.Route("KalfaEczacilar/{id:int}")]
        [System.Web.Http.HttpPut]
        [ResponseType(typeof(LoginEnt.KalfaEczaci))]
        public IHttpActionResult Update(int id, [FromBody] KalfaDTO dto)
        {
            string dosyaUrl = null;
            var UserId = Convert.ToInt32(User.Identity.GetUserId());
            if (!ModelState.IsValid)
            {
                BadRequest(ModelState);
            }
            try
            {
                var data = ctx.KalfaEczaci.Find(id);
                if (dto.Dosya != null || dto.DosyaBase64 != null || dto.DosyaBase64 != "")
                {
                    dosyaUrl = dosyaUrl = FileUploadHelper.LoadFile(dto.DosyaBase64,dto.Dosya==null?null: Path.GetExtension(dto.Dosya.DosyaAdi), dto.Dosya == null ? null : dto.Dosya.DosyaAdi.Substring(0, dto.Dosya.DosyaAdi.LastIndexOf(".", StringComparison.Ordinal)), Convert.ToInt32(User.Identity.GetUserId()), true, null, false);                 
                }
                ctx.Entry(data).CurrentValues.SetValues(dto);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        /// <summary>
        /// İd ye göre KalfaEczaci Bilgisini siler
        /// </summary>
        /// <param name="id"></param>
        /// <param name=""></param>
        /// <returns></returns>
        [System.Web.Http.Route("KalfaEczacilar/{id:int}")]
        [System.Web.Http.HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var data = ctx.KalfaEczaci.Find(id);
            ctx.KalfaEczaci.Remove(data);
            try
            {
                ctx.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Ok(new HttpStatusCodeResult(HttpStatusCode.ExpectationFailed, e.Message));
            }
        }
        /// <summary>
        ///  id ye göre  KalfaEczacinın KisielBilgisini Getirir
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [System.Web.Http.Route("KalfaEczacilar/{id:int}/KisiBilgisi")]
        [System.Web.Http.HttpGet]
        [ResponseType(typeof(LoginEnt.KisiBilgiler))]
        public IHttpActionResult EczaciKisiBilgisi(int id)
        {
            try
            {             
                var result = ctx.KalfaEczaci.Include("Kisi").FirstOrDefault(x => x.Id==id).Kisi;
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        /// <summary>
        /// Database dispose
        /// </summary>
        protected void Dispose()
        {
            ctx.Dispose();
        }
    }
}
