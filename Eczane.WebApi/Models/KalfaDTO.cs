﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Eczane.BL.Entities;

namespace Eczane.WebApi.Models
{
    public class KalfaDTO
    {
        public int Id { get; set; }
        public string MezunOlduguFakulteAdi { get; set; }
        public DateTime? MezunOlduguTarih { get; set; }
        public int? KisiId { get; set; }
        public int? UserId { get; set; }
        public int? SistemPuan { get; set; }
        public  LoginEnt.User User { get; set; }
        public  LoginEnt.KisiBilgiler Kisi { get; set; }
        public  List<LoginEnt.IstekBilgileri> IstekBilgileri { get; set; }
        public int? DosyaId { get; set; }
        public LoginEnt.Dosyalar Dosya { get; set; }
        public List<LoginEnt.Dosyalar> Dosyalar { get; set; }
        public string DosyaBase64 { get; set; }
    }
}