﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Eczane.BL.Entities;

namespace Eczane.WebApi.Models
{
    public class LoginInfoDTO<Tentity>  where  Tentity:class
    {
        public Tentity UserBilgiler { get; set; }
    }
}