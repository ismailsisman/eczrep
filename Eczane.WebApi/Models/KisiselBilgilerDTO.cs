﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Eczane.BL.Entities;

namespace Eczane.WebApi.Models
{
    public class KisiselBilgilerDTO
    {
        public int Id { get; set; }
        public string Ad { get; set; }
        public string SoyAd { get; set; }
        public string FotoUrl { get; set; }
        public string IlId { get; set; }
        public string IlceId { get; set; }
        public string Adres { get; set; }
        public string Telefon { get; set; }
        public DosyaDTO Foto { get; set; }
    }
}