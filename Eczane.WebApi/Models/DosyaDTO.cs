﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eczane.WebApi.Models
{
    public class DosyaDTO
    {
        public int Id { get; set; }
        public string DosyaUrl { get; set; }
        public int UserId { get; set; }
        public string DosyaAdi { get; set; }
        public string DosyaYolu { get; set; }
        public string DosyaBase64 { get; set; }
    }
}