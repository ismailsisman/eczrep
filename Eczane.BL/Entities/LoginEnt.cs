﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
namespace Eczane.BL.Entities
{
    public class LoginEnt
    {
        public class User : IdentityUser<int, UserLogin, UserRole, UserClaim>
        {
            public string Password { get; set; }
            public string Confirim_Password { get; set; }     
            public int? KisiId { get; set; }
            [ForeignKey("KisiId")]
            public virtual KisiBilgiler Kisi { get; set; }
            public int? DosyaId { get; set; }
            [ForeignKey("DosyaId")]
            public virtual List<Dosyalar> Dosyalar { get; set; }
        }
        public class UserLogin : IdentityUserLogin<int>
        {
            public int Id { get; set; }
        }
        public class UserRole : IdentityUserRole<int> { public int Id { get; set; } }
        public class Role : IdentityRole<int, UserRole>
        {
            public virtual ICollection<RoleUrl> RoleUrl { get; set; }
        }
        public class UserClaim : IdentityUserClaim<int> { }
        public class RoleUrl
        {
            [Key]
            public long Id { get; set; }
            //public int Id { get; set; }
            [ForeignKey("Role")]
            //[Key, Column(Order = 0)]
            public int? RoleId { get; set; }
            [ForeignKey("Url")]
            //[Key, Column(Order = 1)]
            public int? UrlId { get; set; }
            public int? UrlApiId { get; set; }
            public virtual Url Url { get; set; }
            public virtual Role Role { get; set; }
            [ForeignKey("UrlApiId")]
            public virtual  UrlApi UrlApi { get; set; }
        }
        public class Url
        {
            public int Id { get; set; }
            public string Area { get; set; }
            public string ActionName { get; set; }
            public string ControlName { get; set; }
            public virtual ICollection<RoleUrl> RoleUrl { get; set; }
        }
        public class UrlApi
        {
            public int Id { get; set; }
            public string MetotName { get; set; }
            public string Url { get; set; }
            public virtual ICollection<RoleUrl> RoleUrl { get; set; }
        }
        public class KisiBilgiler
        {
            [Key]
            public  int Id { get; set; }
            public string Ad { get; set; }
            public string SoyAd { get; set; }          
            public string FotoUrl { get; set; }
            public string IlId { get; set; }
            public string IlceId { get; set; }
            public string Adres { get; set; }
            public string Telefon { get; set; }
        }
        public class Eczaci
        {
            [Key]
            public int Id { get; set; }
            //public string Ad { get; set; }
            //public string SoyAd { get; set; }
            public string GIN_Numarasi { get; set; }
            public int? UserId { get; set; }
            public int? KisiId { get; set; }
            [ForeignKey("UserId")]
            public virtual User User { get; set; }
            [ForeignKey("KisiId")]
            public virtual KisiBilgiler Kisi { get; set; }
            public virtual List<IstekBilgileri> IstekBilgileri { get; set; }
        }
        public class KalfaEczaci
        {
            [Key]
            public int Id { get; set; }
            //public string Ad { get; set; }
            //public string SoyAd { get; set; }
            public string MezunOlduguFakulteAdi { get; set; }
            public DateTime? MezunOlduguTarih { get; set; }
            public int? KisiId { get; set; }
            public int? UserId { get; set; }
            public int? SistemPuan { get; set; }
            [ForeignKey("UserId")]
            public virtual User User { get; set; }
            [ForeignKey("KisiId")]
            public virtual KisiBilgiler Kisi { get; set; }
            public virtual List<IstekBilgileri> IstekBilgileri { get; set; }
        }
        public class Dosyalar
        {
            [Key]
            public int Id { get; set; }
            public string DosyaUrl { get; set; }
            public int UserId { get; set; }
            public string DosyaAdi { get; set; }
            public string DosyaYolu { get; set; }
            [ForeignKey("UserId")]
            public  User User { get; set; }
        }
        public class IstekBilgileri 
        {
            [Key]
            public int Id { get; set; }
            public int? EczId { get; set; }
            public int? KalfaId { get; set; }
            [ForeignKey("KalfaId")]
            public virtual KalfaEczaci KalfaEczaci { get; set; }
            [ForeignKey("EczId")]
            public virtual Eczaci Eczaci { get; set; }
            public bool IstekGonderildiMi { get; set; } = false;
            public DateTime? IstekGonderilmeTarihi { get; set; }
            public DateTime? AdminIstekOnayTarihi { get; set; }          
        }
    }
}
