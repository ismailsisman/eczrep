namespace Eczane.BL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIstekBilgileri : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Dosyalar",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DosyaUrl = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Password = c.String(),
                        Confirim_Password = c.String(),
                        KisiId = c.Int(),
                        DosyaId = c.Int(),
                        Email = c.String(),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.KisiBilgiler", t => t.KisiId)
                .Index(t => t.KisiId);
            
            CreateTable(
                "dbo.UserClaim",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.KisiBilgiler",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ad = c.String(),
                        SoyAd = c.String(),
                        FotoUrl = c.String(),
                        IlId = c.String(),
                        IlceId = c.String(),
                        Adres = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserLogin",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LoginProvider = c.String(),
                        ProviderKey = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserRole",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Role", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Eczaci",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GIN_Numarasi = c.String(),
                        UserId = c.Int(),
                        KisiId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.KisiBilgiler", t => t.KisiId)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.KisiId);
            
            CreateTable(
                "dbo.IstekBilgileri",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EczId = c.Int(),
                        KalfaId = c.Int(),
                        IstekGonderildiMi = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Eczaci", t => t.EczId)
                .ForeignKey("dbo.KalfaEczaci", t => t.KalfaId)
                .Index(t => t.EczId)
                .Index(t => t.KalfaId);
            
            CreateTable(
                "dbo.KalfaEczaci",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MezunOlduguFakulteAdi = c.String(),
                        MezunOlduguTarih = c.DateTime(nullable: false),
                        KisiId = c.Int(),
                        UserId = c.Int(),
                        SistemPuan = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.KisiBilgiler", t => t.KisiId)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.KisiId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RoleUrl",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        RoleId = c.Int(),
                        UrlId = c.Int(),
                        UrlApiId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Role", t => t.RoleId)
                .ForeignKey("dbo.Url", t => t.UrlId)
                .ForeignKey("dbo.UrlApi", t => t.UrlApiId)
                .Index(t => t.RoleId)
                .Index(t => t.UrlId)
                .Index(t => t.UrlApiId);
            
            CreateTable(
                "dbo.Url",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Area = c.String(),
                        ActionName = c.String(),
                        ControlName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UrlApi",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MetotName = c.String(),
                        Url = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRole", "RoleId", "dbo.Role");
            DropForeignKey("dbo.RoleUrl", "UrlApiId", "dbo.UrlApi");
            DropForeignKey("dbo.RoleUrl", "UrlId", "dbo.Url");
            DropForeignKey("dbo.RoleUrl", "RoleId", "dbo.Role");
            DropForeignKey("dbo.Eczaci", "UserId", "dbo.User");
            DropForeignKey("dbo.Eczaci", "KisiId", "dbo.KisiBilgiler");
            DropForeignKey("dbo.KalfaEczaci", "UserId", "dbo.User");
            DropForeignKey("dbo.KalfaEczaci", "KisiId", "dbo.KisiBilgiler");
            DropForeignKey("dbo.IstekBilgileri", "KalfaId", "dbo.KalfaEczaci");
            DropForeignKey("dbo.IstekBilgileri", "EczId", "dbo.Eczaci");
            DropForeignKey("dbo.UserRole", "UserId", "dbo.User");
            DropForeignKey("dbo.UserLogin", "UserId", "dbo.User");
            DropForeignKey("dbo.User", "KisiId", "dbo.KisiBilgiler");
            DropForeignKey("dbo.Dosyalar", "UserId", "dbo.User");
            DropForeignKey("dbo.UserClaim", "UserId", "dbo.User");
            DropIndex("dbo.RoleUrl", new[] { "UrlApiId" });
            DropIndex("dbo.RoleUrl", new[] { "UrlId" });
            DropIndex("dbo.RoleUrl", new[] { "RoleId" });
            DropIndex("dbo.KalfaEczaci", new[] { "UserId" });
            DropIndex("dbo.KalfaEczaci", new[] { "KisiId" });
            DropIndex("dbo.IstekBilgileri", new[] { "KalfaId" });
            DropIndex("dbo.IstekBilgileri", new[] { "EczId" });
            DropIndex("dbo.Eczaci", new[] { "KisiId" });
            DropIndex("dbo.Eczaci", new[] { "UserId" });
            DropIndex("dbo.UserRole", new[] { "RoleId" });
            DropIndex("dbo.UserRole", new[] { "UserId" });
            DropIndex("dbo.UserLogin", new[] { "UserId" });
            DropIndex("dbo.UserClaim", new[] { "UserId" });
            DropIndex("dbo.User", new[] { "KisiId" });
            DropIndex("dbo.Dosyalar", new[] { "UserId" });
            DropTable("dbo.UrlApi");
            DropTable("dbo.Url");
            DropTable("dbo.RoleUrl");
            DropTable("dbo.Role");
            DropTable("dbo.KalfaEczaci");
            DropTable("dbo.IstekBilgileri");
            DropTable("dbo.Eczaci");
            DropTable("dbo.UserRole");
            DropTable("dbo.UserLogin");
            DropTable("dbo.KisiBilgiler");
            DropTable("dbo.UserClaim");
            DropTable("dbo.User");
            DropTable("dbo.Dosyalar");
        }
    }
}
