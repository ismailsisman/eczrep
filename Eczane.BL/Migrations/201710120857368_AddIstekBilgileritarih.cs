namespace Eczane.BL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIstekBilgileritarih : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.IstekBilgileri", "IstekGonderilmeTarihi", c => c.DateTime(nullable: false));
            AddColumn("dbo.IstekBilgileri", "AdminIstekOnayTarihi", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.IstekBilgileri", "AdminIstekOnayTarihi");
            DropColumn("dbo.IstekBilgileri", "IstekGonderilmeTarihi");
        }
    }
}
