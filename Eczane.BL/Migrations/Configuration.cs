using System.Linq.Expressions;
using System.Runtime.InteropServices.ComTypes;
using System.Web.Configuration;
using Eczane.BL.Entities;

namespace Eczane.BL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Eczane.BL.Manage.EczaneDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(Eczane.BL.Manage.EczaneDbContext context)
        {
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            if (!context.UrlApi.Any())
            {
                context.UrlApi.AddOrUpdate(
                     new LoginEnt.UrlApi(){ MetotName = "GET", Url = "api/Eczacilar" },
                     new LoginEnt.UrlApi() { MetotName = "POST", Url = "api/Eczacilar" },
                     new LoginEnt.UrlApi() { MetotName = "DELETE", Url = "api/Eczacilar" },
                     new LoginEnt.UrlApi() { MetotName = "PUT", Url = "api/Eczacilar" });
                context.SaveChanges();
                //new LoginEnt.UrlApi() {MetotName = "GET", Url = "api/Eczacilar"};
                //new LoginEnt.UrlApi() { MetotName = "POST", Url = "api/Eczacilar" };
                //new LoginEnt.UrlApi() { MetotName = "DELETE", Url = "api/Eczacilar" };
                //new LoginEnt.UrlApi() { MetotName = "PUT", Url = "api/Eczacilar" };
            }
            if (!context.User.Any())
            {
                context.Url.AddOrUpdate(new LoginEnt.Url(){Area = "Admin",ActionName = "Index",ControlName = "Rol_Islem"});
                context.Url.AddOrUpdate(new LoginEnt.Url() { Area = "Admin", ActionName = "GetAllRole", ControlName = "Rol_Islem" });
                context.Url.AddOrUpdate(new LoginEnt.Url() { Area = "Admin", ActionName = "RoleEkle", ControlName = "Rol_Islem" });
                context.Url.AddOrUpdate(new LoginEnt.Url() { Area = "Admin", ActionName = "RolDuzenle", ControlName = "Rol_Islem" });
                context.Url.AddOrUpdate(new LoginEnt.Url() { Area = "Admin", ActionName = "GetRole", ControlName = "Rol_Islem" });
                context.Url.AddOrUpdate(new LoginEnt.Url() { Area = "Admin", ActionName = "DeleteRole", ControlName = "Rol_Islem" });
                context.Url.AddOrUpdate(new LoginEnt.Url() { Area = "Admin", ActionName = "KullaniciyaRolAta", ControlName = "Rol_Islem" });
                context.Url.AddOrUpdate(new LoginEnt.Url() { Area = "Admin", ActionName = "KullaniciyaRoleAtaKaydet", ControlName = "Rol_Islem" });
                context.Url.AddOrUpdate(new LoginEnt.Url() { Area = "Admin", ActionName = "RoleYetkiAtama", ControlName = "Rol_Islem" });
                context.Url.AddOrUpdate(new LoginEnt.Url() { Area = "Admin", ActionName = "Index", ControlName = "EczaciKalfa" });
                context.Url.AddOrUpdate(new LoginEnt.Url() { Area = "Admin", ActionName = "IstekYap", ControlName = "EczaciKalfa" });

                context.Url.AddOrUpdate(new LoginEnt.Url() { Area = "Eczaci", ActionName = "Index", ControlName = "EczaciIslem" });
                context.Url.AddOrUpdate(new LoginEnt.Url() { Area = "Eczaci", ActionName = "EczaciBilgileri", ControlName = "EczaciIslem" });
                context.Url.AddOrUpdate(new LoginEnt.Url() { Area = "Eczaci", ActionName = "GetKalfaBilgileri", ControlName = "EczaciIslem" });
                context.Url.AddOrUpdate(new LoginEnt.Url() { Area = "Eczaci", ActionName = "IstekYap", ControlName = "EczaciIslem" });
                context.Url.AddOrUpdate(new LoginEnt.Url() { Area = "Eczaci", ActionName = "GuncelleEczaciBilgileri", ControlName = "EczaciIslem" });

                context.Url.AddOrUpdate(new LoginEnt.Url() { Area = "KalfaEczaci", ActionName = "Index", ControlName = "KalfaEczaciIslem" });
                context.Url.AddOrUpdate(new LoginEnt.Url() { Area = "KalfaEczaci", ActionName = "Ekle", ControlName = "KalfaEczaciIslem" });
                context.Url.AddOrUpdate(new LoginEnt.Url() { Area = "KalfaEczaci", ActionName = "Guncelle", ControlName = "KalfaEczaciIslem" });
                context.Url.AddOrUpdate(new LoginEnt.Url() { Area = "KalfaEczaci", ActionName = "Download", ControlName = "KalfaEczaciIslem" });
                context.SaveChanges();
                context.Role.AddOrUpdate(new LoginEnt.Role(){Name = "Admin"});
                context.SaveChanges();
                context.User.AddOrUpdate(new LoginEnt.User() { Email = "admin@admin", UserName = "admin@admin", Password = "123456789", Confirim_Password = "123456789", PasswordHash = "ABMsvU7GX4+j2g1xIBQ7gz7x7QDJ4CU2PEdVFTPLg4pIhZyPjNWr+i4b9RR3r6oejA==", SecurityStamp = "00af8ac5-9a70-4926-a0ae-2c00cfb6c0e0", });
                context.SaveChanges();
                context.UserRole.AddOrUpdate(new LoginEnt.UserRole()
                {
                    RoleId = context.Role.FirstOrDefault().Id,
                    UserId =context.User.FirstOrDefault().Id 
                });               
                context.SaveChanges();
                foreach (var item in context.Url.ToList())
                {
                    context.RoleUrl.Add(new LoginEnt.RoleUrl()
                    {
                        RoleId = context.Role.FirstOrDefault().Id,
                        UrlId = item.Id
                    });
                }
                context.SaveChanges();
            }
        }
    }
}
