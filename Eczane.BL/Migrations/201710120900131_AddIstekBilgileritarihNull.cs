namespace Eczane.BL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIstekBilgileritarihNull : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.IstekBilgileri", "IstekGonderilmeTarihi", c => c.DateTime());
            AlterColumn("dbo.IstekBilgileri", "AdminIstekOnayTarihi", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.IstekBilgileri", "AdminIstekOnayTarihi", c => c.DateTime(nullable: false));
            AlterColumn("dbo.IstekBilgileri", "IstekGonderilmeTarihi", c => c.DateTime(nullable: false));
        }
    }
}
