﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Channels;
using System.Web;
using System.Web.Configuration;
using System.Web.Helpers;
using System.Web.Http;
using Eczane.BL.Entities;
using Eczane.BL.Manage;

namespace Eczane.BL.Helpers
{
    public static class FileUploadHelper
    {
        public static string LoadFile(string Base64, string fileType, string DosyaAdi, int UserId, bool baseOrByte,
            HttpPostedFileBase file, bool ProfilFotosuMu = false)
        {
            string DosyaYolu = "";
            string pathToFiles = HttpContext.Current.Server.MapPath("/" + WebConfigurationManager.AppSettings["DosyaKayitYeri"]);
            LoginEnt.Dosyalar dosya = new LoginEnt.Dosyalar();
            if (baseOrByte)
            {
                var newpath = pathToFiles.Substring(0, pathToFiles.IndexOf("EczaneWeb") + 9) + "\\" + "Eczane.WebUI\\" +
                              WebConfigurationManager.AppSettings["DosyaKayitYeri"];
                byte[] bytes = Convert.FromBase64String(Base64);
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    using (var ctx = new EczaneDbContext())
                    {
                        //dosya.UserId = UserId;
                        //dosya.DosyaUrl = requiredPath+"\\"+ WebConfigurationManager.AppSettings["DosyaKayitYeri"] +"\\"+
                        //    Guid.NewGuid().ToString() + "_" + UserId + "_" +
                        //             fileType;
                        var guid = Guid.NewGuid();
                        DosyaYolu = Path.Combine(newpath.Replace("\\", "/") + "/" +
                                                 guid + "_" + UserId + "_" +
                                         fileType);
                        dosya.DosyaUrl = Path.Combine("/" + WebConfigurationManager.AppSettings["DosyaKayitYeri"] + "/" +
                                                     guid + "_" + UserId + "_" +
                                                     fileType);
                        dosya.UserId = UserId;
                        dosya.DosyaYolu = DosyaYolu;
                        dosya.DosyaAdi = DosyaAdi;
                        ctx.Dosya.Add(dosya);
                        ctx.SaveChanges();
                    }
                    if (!ProfilFotosuMu)
                    {
                        using (var dosyafile = new FileStream(DosyaYolu, FileMode.Create))
                        {
                            dosyafile.Write(bytes, 0, bytes.Length);
                            dosyafile.Flush();
                        }
                    }
                    else
                    {
                        using (Bitmap bm2 = new Bitmap(ms))
                        {
                            var dosyaYoluKayit = DosyaYolu.Replace("/", "\\");
                            var image = Image.FromStream(ms);
                            var newImage = ScaleImage(image, 175, 175);                        
                            newImage.Save(dosyaYoluKayit);
                        }
                    }
                }
                return dosya.DosyaUrl;
            }
            else
            {
                var byte_ = ReadFully(file.InputStream);
                var actualFileName = file.FileName;
                Dictionary<string, string> fileAttributes = new Dictionary<string, string>();
                fileAttributes.Add("UserId", UserId.ToString());
                fileAttributes.Add("GercekAdi", actualFileName);
                using (MemoryStream ms = new MemoryStream(byte_))
                {
                    using (var ctx = new EczaneDbContext())
                    {
                        //dosya.UserId = UserId;
                        var guid = Guid.NewGuid();
                        DosyaYolu = Path.Combine(pathToFiles.Replace("\\", "/") + "/" +
                                                 guid + "_" + UserId + "_" +
                                                 fileType);
                        dosya.DosyaUrl = Path.Combine("/" + WebConfigurationManager.AppSettings["DosyaKayitYeri"] + "/" +
                                                      guid + "_" + UserId + "_" +
                                                      fileType);
                        dosya.UserId = UserId;
                        dosya.DosyaYolu = DosyaYolu;
                        dosya.DosyaAdi = DosyaAdi;
                        ctx.Dosya.Add(dosya);
                        ctx.SaveChanges();
                    }

                    if (!ProfilFotosuMu)
                    {
                        file.SaveAs(DosyaYolu.Replace("/", "\\"));
                    }
                    else
                    {
                        using (Bitmap bm2 = new Bitmap(ms))
                        {
                            if (ProfilFotosuMu)
                            {
                                var image = Image.FromStream(ms);
                                var newImage = ScaleImage(image, 175, 175);
                                var dosyaYoluKayit = DosyaYolu.Replace("/", "\\");
                                newImage.Save(dosyaYoluKayit);
                            }                        
                        }
                    }
                }
                return dosya.DosyaUrl;
            }
        }
        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        public static Image ScaleImage(Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);

            using (var graphics = Graphics.FromImage(newImage))
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);

            return newImage;
        }
    }
}