﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Eczane.BL.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace Eczane.BL.Manage
{
    public class UserStoreApp : UserStore<LoginEnt.User, LoginEnt.Role, int, LoginEnt.UserLogin, LoginEnt.UserRole, LoginEnt.UserClaim>
    {
        public UserStoreApp(EczaneDbContext ctx)
            : base(ctx)
        {
        }
    }
    public class UserManager : UserManager<LoginEnt.User, int>
    {
        public UserManager(UserStoreApp store) : base(store)
        {
        }
        public static UserManager CreateCallback(IdentityFactoryOptions<UserManager> identityFactoryOptions, IOwinContext owinContext)
        {
            EczaneDbContext context = owinContext.Get<EczaneDbContext>();
            UserManager Manage = new UserManager(new UserStoreApp(context));
            Manage.PasswordValidator = new PasswordValidator
            {               
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            }; var dataProtectionProvider = identityFactoryOptions.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                Manage.UserTokenProvider = new DataProtectorTokenProvider<LoginEnt.User,int>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return Manage;
        }
    }
    public class RoleStoreApp : RoleStore<LoginEnt.Role, int, LoginEnt.UserRole>
    {
        public RoleStoreApp(EczaneDbContext ctx)
            : base(ctx)
        {
        }
    }
    public class RoleManager : RoleManager<LoginEnt.Role, int>
    {
        public RoleManager(RoleStoreApp store) : base(store)
        {

        }
        public static RoleManager CreateCallback(IdentityFactoryOptions<RoleManager> identityFactoryOptions, IOwinContext owinContext)
        {
            EczaneDbContext context = owinContext.Get<EczaneDbContext>();
            RoleManager roleManager = new RoleManager(new RoleStoreApp(context));
            return roleManager;
        }
    }
    public class ApplicationSignInManager : SignInManager<LoginEnt.User, int>
    {       
        public ApplicationSignInManager(UserManager<LoginEnt.User, int> userManager, IAuthenticationManager authenticationManager) : base(userManager, authenticationManager)
        {
        }
        //public static Task<ClaimsIdentity> CreateUserIdentity(LoginEnt.User user, UserManager userManager)
        //{
        //    return userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
        //}
        public override Task<ClaimsIdentity> CreateUserIdentityAsync(LoginEnt.User user)
        {
            return CreateUserIdentityClass.GenerateUserIdentityAsync(user);
        }
        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<UserManager>(), context.Authentication);
        }
    }
    public static class   CreateUserIdentityClass
    {
        public static async Task<ClaimsIdentity> GenerateUserIdentityAsync(LoginEnt.User user,string authenticationType=null)
        { 
            UserManager userManager = new UserManager(new UserStoreApp(EczaneDbContext.Create()));
            var userIdentity= await userManager.CreateIdentityAsync(user, authenticationType==null?DefaultAuthenticationTypes.ApplicationCookie: authenticationType);
            return userIdentity;
        }      
    }
}
