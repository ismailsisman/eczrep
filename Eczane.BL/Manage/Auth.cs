﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Routing;
using System.Web.UI.WebControls.WebParts;
using Eczane.BL.Entities;
using Eczane.BL.Manage;
using Microsoft.AspNet.Identity;
using AllowAnonymousAttribute = System.Web.Mvc.AllowAnonymousAttribute;
using FilterAttribute = System.Web.Mvc.FilterAttribute;
using IAuthorizationFilter = System.Web.Mvc.IAuthorizationFilter;
using RedirectToRouteResult = System.Web.Mvc.RedirectToRouteResult;

namespace Eczane.BL.Manage
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    public class AuthApi : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext filterContext)
        {
            var routeData = filterContext.Request.GetRouteData();
            var UrlName = routeData.Route.RouteTemplate;
            var Metot = filterContext.ActionDescriptor.SupportedHttpMethods.FirstOrDefault().Method;
            var ControllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var ActionName = filterContext.ActionDescriptor.ActionName;
            var AllowControl= filterContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any()
                || filterContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();
            //var Area = filterContext.ControllerContext.RouteData.Route.DataTokens["area"];
            //Area = Area != null ? Area.ToString() : "";
            if (!AllowControl)
            {
                if (!ContollerRole.IsInRole(Metot, UrlName))
                {
                   filterContext.Response= new HttpResponseMessage(HttpStatusCode.Unauthorized);  
                }
            }            
        }
        public class ContollerRole
        {
            public static bool IsInRole(string Metot, string UrlName)
            {
                LoginEnt.UrlApi url = new LoginEnt.UrlApi()
                {
                     MetotName = Metot,
                     Url = UrlName
                };
                List<LoginEnt.UrlApi> UrlList = new List<LoginEnt.UrlApi>();
                var UserId = HttpContext.Current.User.Identity.GetUserId();
                var UserName = HttpContext.Current.User.Identity.Name;
                if (UserId != null)
                {
                    using (var db = new EczaneDbContext())
                    {
                        int usId = Convert.ToInt32(UserId);
                        var RoleId = db.UserRole.Where(x => x.UserId == usId).Select(x => x.RoleId).ToList();

                        foreach (var role in RoleId)
                        {
                            UrlList.AddRange(db.RoleUrl.Where(x => x.RoleId == role).Select(x => x.UrlApi).ToList());
                        }
                    }                 
                    var result = UrlList.Any(x => x.Url == UrlName &&
                                                  x.MetotName == Metot);
                    return result;
                }
                else
                {
                    return false;
                }
            }
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = false, AllowMultiple = true)]
    public class Auth : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var ControllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var ActionName = filterContext.ActionDescriptor.ActionName;
            var Area = filterContext.RouteData.DataTokens["area"];
            Area = Area != null ? Area.ToString() : "";
            if (!filterContext.ActionDescriptor.IsDefined
                    (typeof(AllowAnonymousAttribute), true) &&
                !filterContext.ActionDescriptor.ControllerDescriptor.IsDefined
                    (typeof(AllowAnonymousAttribute), true))
            {
                if (!ContollerRole.IsInRole(Area.ToString(), ActionName, ControllerName))
                {
                    ViewResult vr = new ViewResult();
                    vr.ViewName = "~/Views/Hata.cshtml";
                    filterContext.Result = vr;
                }
            }
        }

        protected void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result =
                new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                        {"controller", "Account"},
                        {"action", "Login"}
                    });
        }

        public class ContollerRole
        {
            public static bool IsInRole(string Area, string ActionName, string ContollerName)
            {
                LoginEnt.Url url = new LoginEnt.Url()
                {
                    Area = Area,
                    ActionName = ActionName,
                    ControlName = ContollerName
                };
                List<LoginEnt.Url> UrlList = new List<LoginEnt.Url>();
                var UserId = HttpContext.Current.User.Identity.GetUserId();
                if (UserId != null)
                {
                    using (var db = new EczaneDbContext())
                    {
                        int usId = Convert.ToInt32(UserId);
                        var RoleId = db.UserRole.Where(x => x.UserId == usId).Select(x => x.RoleId).ToList();

                        foreach (var role in RoleId)
                        {
                            UrlList.AddRange(db.RoleUrl.Where(x => x.RoleId == role).Select(x => x.Url).ToList());
                        }
                    }
                    Area = Area == "" ? null : Area;
                    var result = UrlList.Any(x => x.Area == Area && x.ActionName == ActionName &&
                                                  x.ControlName == ContollerName);
                    return result;
                }
                else
                {
                    return true;
                }
            }
        }
    }
}

