﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eczane.BL.Entities;

namespace Eczane.BL.Manage
{
   public  class EczaneDbContext:DbContext
    {
       public  EczaneDbContext():base("EczaneDb")
        {
            Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer<EczaneDbContext>(new CreateDatabaseIfNotExists<EczaneDbContext>());
        }
        public static EczaneDbContext Create()
        {
            try
            {             
                return new EczaneDbContext();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }                  
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<LoginEnt.User>().ToTable("User");
            modelBuilder.Entity<LoginEnt.Role>().ToTable("Role");
            modelBuilder.Entity<LoginEnt.UserRole>().ToTable("UserRole");
            modelBuilder.Entity<LoginEnt.UserClaim>().ToTable("UserClaim");
            modelBuilder.Entity<LoginEnt.UserLogin>().ToTable("UserLogin");
            modelBuilder.Entity<LoginEnt.Url>().ToTable("Url");
            modelBuilder.Entity<LoginEnt.RoleUrl>().ToTable("RoleUrl");
        }
        public DbSet<LoginEnt.User> User { get; set; }
        public DbSet<LoginEnt.Role> Role { get; set; }
        public DbSet<LoginEnt.UserRole> UserRole { get; set; }
        public DbSet<LoginEnt.UserClaim> UserClaim { get; set; }
        public DbSet<LoginEnt.UserLogin> UserLogin { get; set; }
        public DbSet<LoginEnt.Url> Url { get; set; }
        public DbSet<LoginEnt.RoleUrl> RoleUrl { get; set; }
        public DbSet<LoginEnt.KisiBilgiler> KisiBilgiler { get; set; }
        public DbSet<LoginEnt.Eczaci> Eczaci { get; set; }
        public DbSet<LoginEnt.KalfaEczaci> KalfaEczaci { get; set; }
        public DbSet<LoginEnt.UrlApi> UrlApi { get; set; }
        public DbSet<LoginEnt.Dosyalar> Dosya { get; set; }
        public DbSet<LoginEnt.IstekBilgileri> IstekBilgileri { get; set; }
    }
}
