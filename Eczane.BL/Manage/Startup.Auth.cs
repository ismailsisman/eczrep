﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eczane.BL.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataHandler;
using Microsoft.Owin.Security.OAuth;
using Owin;

namespace Eczane.BL.Manage
{
    public partial class Startup
    {
        //public OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        // For more information on configuring authentication, please visit https://go.microsoft.com/fwlink/?LinkId=301864
        public  void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(EczaneDbContext.Create);
            app.CreatePerOwinContext<UserManager>(UserManager.CreateCallback);
            app.CreatePerOwinContext<RoleManager>(RoleManager.CreateCallback);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            //app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions
            //{

            //});
            //var PublicClientId = "self";
            // OAuthOptions = new OAuthAuthorizationServerOptions
            // {
            //     TokenEndpointPath = new PathString("/Token"),
            //     Provider = new OAuthAuthorizationServerProvider(),
            //     AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
            //     AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
            //     // Note: Remove the following line before you deploy to production:
            //     AllowInsecureHttp = true
            // };          
            // Enable the application to use bearer tokens to authenticate users
            //app.UseOAuthBearerTokens(OAuthOptions);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),               
                //Provider = new CookieAuthenticationProvider
                //{
                //    //         Enables the application to validate the security stamp when the user logs in.
                //    //         This is a security feature which is used when you change a password or add an external login to your account.
                //    //OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<UserManager, LoginEnt.User>
                //    //(
                //    //        validateInterval: TimeSpan.FromMinutes(30),
                //    //        regenerateIdentity: (manager, user) => CreateUserIdentityClass.GenerateUserIdentityAsync(user))
                //}

            });

          
  
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //   consumerKey: "",
            //   consumerSecret: "");

            //app.UseFacebookAuthentication(
            //   appId: "",
            //   appSecret: "");

            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "",
            //    ClientSecret = ""
            //});
        }
    }
}
