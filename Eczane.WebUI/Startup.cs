﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Eczane.WebUI.Startup))]
namespace Eczane.WebUI
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            BL.Manage.Startup st = new BL.Manage.Startup();
            st.ConfigureAuth(app);
        }
    }
}
