﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.Web.WebPages;
using Eczane.BL.Entities;
using Eczane.BL.Manage;
using Eczane.BL.Helpers;
using Microsoft.AspNet.Identity;

namespace Eczane.WebUI.Areas.User.Controllers
{
    [AllowAnonymous]
    public class KisiBilgileriController : Controller
    {
        private EczaneDbContext ctx;
        public KisiBilgileriController()
        {
            ctx=new EczaneDbContext();
        }
        // GET: User/KisiBilgileri
        public ActionResult Index()
        {          
            var id = Convert.ToInt32(User.Identity.GetUserId());
            var KisiBilgileri = ctx.User.Include("Kisi").FirstOrDefault(x => x.Id == id).Kisi;
            return View(KisiBilgileri);
        }
        [HttpPost]
        public JsonResult KisiBilgileriKaydet(LoginEnt.KisiBilgiler dto)
        {
            var userId = Convert.ToInt32(User.Identity.GetUserId());
            //if (!ModelState.IsValid)
            //{
            //    return Json("Model Geçerli Değil");
            //}
            string dosyaUrl = null;
            var file = Request.Files[0];
            try
            {
                if (ctx.User.Where(p=> p.Id== userId).Any(x=> x.Kisi!=null))
                {
                    return Json("Bu Kişinin Kullanıcı Bilgileri Eklidir O yüzden ekleme yapamazsınız!");
                }
                if (file.ContentLength != 0)
                {
                     dosyaUrl = FileUploadHelper.LoadFile(null, Path.GetExtension(file.FileName), file.FileName.Substring(0, file.FileName.LastIndexOf(".")), Convert.ToInt32(User.Identity.GetUserId()), false, file,true);
                }   
                dto.FotoUrl = dosyaUrl;
                var result = ctx.KisiBilgiler.Add(dto);
                ctx.SaveChanges();
                ctx.User.Include("Kisi").First(x => x.Id ==userId).KisiId= result.Id;
                var eczaci = ctx.Eczaci.FirstOrDefault(x => x.UserId == userId);
                if (eczaci != null)
                    eczaci.KisiId = result.Id;
                var KalfaEczaci = ctx.KalfaEczaci.FirstOrDefault(x => x.UserId == userId);
                if (KalfaEczaci != null)
                    KalfaEczaci.KisiId = result.Id;
                ctx.SaveChanges();
                return Json("Kayıt Başarılı");
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }
        [HttpPost]
        public JsonResult Guncelle(LoginEnt.KisiBilgiler dto)
        {
            var userId = Convert.ToInt32(User.Identity.GetUserId());
            if (!ModelState.IsValid)
            {
                return Json("Model Geçerli Değil");
            }
            var file = Request.Files[0];
            if (file.ContentLength!=0)
            {
                string pathToFiles = Server.MapPath("/");
                
                if (ctx.User.Include("Kisi").Any(x => x.Kisi.FotoUrl != null))
                {
                    var eskiFoto = ctx.User.Include("Kisi").FirstOrDefault(x => x.Id == userId).Kisi.FotoUrl;
                    if (eskiFoto!=null)
                    {
                        var dosyaYolu = ctx.Dosya.FirstOrDefault(c => c.DosyaUrl == eskiFoto).DosyaUrl.Remove(0, 1);
                        if (System.IO.File.Exists(@pathToFiles + dosyaYolu.Replace("/", "\\")))
                        {
                            System.IO.File.Delete(@pathToFiles + dosyaYolu.Replace("/", "\\"));
                        }
                    }
                }             
                var dosyaUrl = FileUploadHelper.LoadFile(null, Path.GetExtension(file.FileName), file.FileName.Substring(0, file.FileName.LastIndexOf(".")), Convert.ToInt32(User.Identity.GetUserId()), false, file, true);
                dto.FotoUrl = dosyaUrl;
            }
            try
            {                         
                var result = ctx.User.Include("Kisi").FirstOrDefault(x=> x.Id== userId && x.KisiId==dto.Id)?.Kisi;
                ctx.Entry(result).CurrentValues.SetValues(dto);
                ctx.SaveChanges();              
                return Json(dto);
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }
    }
}