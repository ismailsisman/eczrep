﻿using System.Web.Mvc;

namespace Eczane.WebUI.Areas.KalfaEczaci
{
    public class KalfaEczaciAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "KalfaEczaci";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "KalfaEczaci_default",
                "KalfaEczaci/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}