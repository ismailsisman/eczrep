﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Eczane.BL.Entities;
using Eczane.BL.Helpers;
using Eczane.BL.Manage;
using Microsoft.AspNet.Identity;

namespace Eczane.WebUI.Areas.KalfaEczaci.Controllers
{
    public class KalfaEczaciIslemController : Controller
    {
        private readonly EczaneDbContext ctx;
        public KalfaEczaciIslemController()
        {
            ctx=new EczaneDbContext();
        }
        // GET: KalfaEczaci/KalfaEczaciIslem
        public ActionResult Index()
        {
            var userId = Convert.ToInt32(User.Identity.GetUserId());
            var result = ctx.KalfaEczaci.Include("Kisi").Include("User").Include("User.Dosyalar").FirstOrDefault(x => x.UserId == userId);
            return View(result);
        }
        public JsonResult  Ekle(LoginEnt.KalfaEczaci dto)
        {
            if (!ModelState.IsValid)
            {
                return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest));
            }
            dto.UserId= Convert.ToInt32(User.Identity.GetUserId());
            try
            {
                string dosyaUrl = null;
                var file = Request.Files[0];
                if (file.ContentLength != 0)
                {
                     dosyaUrl = FileUploadHelper.LoadFile(null, Path.GetExtension(file.FileName), file.FileName.Substring(0, file.FileName.LastIndexOf(".")), Convert.ToInt32(User.Identity.GetUserId()), false, file, false);
                }
                var ent = ctx.KalfaEczaci.Include("User").Include("User.Dosyalar").FirstOrDefault(x=> x.Id==dto.Id);
                ctx.Entry(ent).CurrentValues.SetValues(dto);
                ctx.SaveChanges();
                return Json("Kayıt Başarılı");
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }
        public JsonResult Guncelle(LoginEnt.KalfaEczaci dto)
        {
            if (!ModelState.IsValid)
            {
                return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest));
            }
            try
            {
                string dosyaUrl = null;
                var file = Request.Files[0];
                if (file.ContentLength != 0)
                {
                    dosyaUrl = FileUploadHelper.LoadFile(null, Path.GetExtension(file.FileName), file.FileName.Substring(0, file.FileName.LastIndexOf(".")), Convert.ToInt32(User.Identity.GetUserId()), false, file, false);
                }
                dto.UserId = Convert.ToInt32(User.Identity.GetUserId());
                var ent = ctx.KalfaEczaci.Find(dto.Id);
                ctx.Entry(ent).CurrentValues.SetValues(dto);
                ctx.SaveChanges();
                return Json("Kayıt Başarılı");
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }     
        public FileResult Download(int Id)
        {
            var dosya = ctx.Dosya.Find(Id);
            byte[] fileBytes = System.IO.File.ReadAllBytes(dosya.DosyaYolu);
            string fileName = Path.GetExtension(dosya.DosyaYolu);
            return File(fileBytes, "text/html", dosya.DosyaAdi+fileName);
        }
    }
}