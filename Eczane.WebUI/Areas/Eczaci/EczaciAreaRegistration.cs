﻿using System.Web.Mvc;

namespace Eczane.WebUI.Areas.Eczaci
{
    public class EczaciAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Eczaci";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Eczaci_default",
                "Eczaci/{controller}/{action}/{id}",
                new { action = "EczaciIndex", id = UrlParameter.Optional }
            );
        }
    }
}