﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http.Results;
using System.Web.Management;
using System.Web.Mvc;
using Eczane.BL.Entities;
using Eczane.BL.Manage;
using Eczane.WebApi.Models;
using Eczane.WebUI.Models;
using Microsoft.AspNet.Identity;

namespace Eczane.WebUI.Areas.Eczaci.Controllers
{
    public class EczaciIslemController : Controller
    {
        private readonly EczaneDbContext ctx;
        public EczaciIslemController()
        {
            ctx=new EczaneDbContext();
        }
        public ActionResult EczaciIndex()
        {
            var result = ctx.KalfaEczaci.Include("Kisi").OrderBy(x=> x.SistemPuan).Take(10).Select(x=> new KalfaDTO()
            {
                Kisi = x.Kisi,MezunOlduguFakulteAdi = x.MezunOlduguFakulteAdi,
                MezunOlduguTarih = x.MezunOlduguTarih,
                Id = x.Id
            }).ToList();
            return View(result);
        }
        // GET: Eczaci/EczaciIslem
        public ActionResult Index()
        {
            //LoginEnt.KalfaEczaci result=null;
            //var client = new HttpClient();
            //client.BaseAddress = new Uri("http://localhost:57939/");
            //client.DefaultRequestHeaders.Accept.Clear();
            //client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "");
            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //var response =  client.GetAsync("api/KalfaEczacilar/Search").Result;
            //if (response.IsSuccessStatusCode)
            //{
            //    result = response.Content.ReadAsAsync<LoginEnt.KalfaEczaci>().Result;
            //}
            var result = ctx.KalfaEczaci.Include("User").Include("Kisi").ToList();
            return View(result);
        }
        public ActionResult EczaciBilgileri()
        {
            if (ctx.Eczaci.Count() != 0)
            {
                var userId = Convert.ToInt32(User.Identity.GetUserId());
                var result = ctx.Eczaci.Include("Kisi")
                    .FirstOrDefault(x => x.UserId == userId);
                return View(result);
            }
            return View();
        }
        [HttpPost]
        public ActionResult EczaciBilgileri(LoginEnt.Eczaci dto)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest) ;
            }
            var userId = Convert.ToInt32(User.Identity.GetUserId());
            dto.UserId = userId;
             ctx.Entry(ctx.Eczaci.Where(x => x.UserId == userId).First()).CurrentValues
                .SetValues(dto);
            ctx.SaveChanges();
            return View();
        }
        public JsonResult GuncelleEczaciBilgileri(LoginEnt.Eczaci dto)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json("Veriler Hatalı");
                }
                var userId = Convert.ToInt32(User.Identity.GetUserId());
                dto.UserId = userId;
                var result = ctx.Eczaci.FirstOrDefault(x=> x.Id==dto.Id);
                ctx.Entry(result).CurrentValues.SetValues(dto);
                ctx.SaveChanges();
                return Json("Guncelleme Başarılı");
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }
        public JsonResult GetKalfaBilgileri(int Id)
        {
            try
            {
                var result=ctx.KalfaEczaci.Include("Kisi").Include("User.Dosyalar").FirstOrDefault(x=> x.Id==Id);
                EczaciKalfaAramaDTO dto= new EczaciKalfaAramaDTO();
                dto.Kisi = result.Kisi;
                dto.Dosyalar = result.User.Dosyalar;
                foreach (var item in dto.Dosyalar)
                {
                    item.User = null;
                }
                dto.Kalfa = result;
                dto.Kalfa.User = null;
                return Json(dto, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }
        public JsonResult IstekYap(int kalfaId)
        {
            try
            {
                var userId = Convert.ToInt32(User.Identity.GetUserId());
                var eczId = ctx.Eczaci.FirstOrDefault(x=> x.UserId==userId).Id;
                LoginEnt.IstekBilgileri istek= new LoginEnt.IstekBilgileri()
                {
                    AdminIstekOnayTarihi = null,
                    EczId = eczId,
                    KalfaId = kalfaId,
                    IstekGonderilmeTarihi = DateTime.Now 
                };
                ctx.IstekBilgileri.Add(istek);
                ctx.SaveChanges();
                return Json("İstek Yapıldı.",JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }
        public ActionResult KalfaBigileri(int id)
        {
            var result = ctx.KalfaEczaci.Include("User").Include("Kisi").Include("Dosya").Select(x=> new KalfaDTO()
            {
                Kisi = x.Kisi,
                Dosyalar = x.User.Dosyalar,
                MezunOlduguFakulteAdi = x.MezunOlduguFakulteAdi,
                MezunOlduguTarih = x.MezunOlduguTarih,
                SistemPuan = x.SistemPuan,
                Id = x.Id
            }).FirstOrDefault();
            return View(result);
        }
    }
}