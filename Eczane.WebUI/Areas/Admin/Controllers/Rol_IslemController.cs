﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Http.Results;
using System.Web.Mvc;
using Eczane.BL.Entities;
using Eczane.BL.Manage;

namespace Eczane.WebUI.Areas.Admin.Controllers
{
    public class Rol_IslemController : Controller
    {
        private EczaneDbContext ctx;
        public Rol_IslemController()
        {
            ctx = new EczaneDbContext();
        }
        // GET: Admin/Rol_Islem
        public ActionResult Index()
        {
            return View(ctx.Role.AsNoTracking().ToList());
        }
        public JsonResult GetAllRole()
        {
            return Json(ctx.Role.ToList(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult RoleEkle(LoginEnt.Role dto)
        {
            if (!ModelState.IsValid || (string.IsNullOrEmpty(dto.Name) || string.IsNullOrWhiteSpace(dto.Name)))
                return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Data is not invalid"));
            try
            {
                var result = ctx.Role.Add(dto);
                ctx.SaveChanges();
                return Json(new HttpStatusCodeResult(HttpStatusCode.OK, "Adding Data is successful"));
            }
            catch (Exception e)
            {
                return Json(new HttpStatusCodeResult(HttpStatusCode.ExpectationFailed, e.Message));
            }
        }
        public JsonResult RolDuzenle(LoginEnt.Role dto)
        {
            if (!ModelState.IsValid || (string.IsNullOrEmpty(dto.Name) || string.IsNullOrWhiteSpace(dto.Name)))
                return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Data is not invalid"));
            try
            {
                var data = ctx.Role.Find(dto.Id);
                ctx.Entry(data).CurrentValues.SetValues(dto);
                //ctx.SaveChanges();
                return Json(new HttpStatusCodeResult(HttpStatusCode.OK, "Updating Data is successful"));
            }
            catch (Exception e)
            {
                return Json(new HttpStatusCodeResult(HttpStatusCode.ExpectationFailed, e.Message));
            }
        }
        public JsonResult GetRole(int Id)
        {
            if (!ModelState.IsValid || Id == null || Id == 0)
                return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Data is not invalid"));
            try
            {
                var result = ctx.Role.Find(Id);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new HttpStatusCodeResult(HttpStatusCode.ExpectationFailed, e.Message));
            }
        }
        public JsonResult DeleteRole(int id)
        {
            if (!ModelState.IsValid || id == null || id == 0)
                return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Data is not invalid"));
            try
            {
                var result = ctx.Role.Find(id);
                ctx.Role.Remove(result);
                ctx.SaveChanges();
                return Json("Veri silindi", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new HttpStatusCodeResult(HttpStatusCode.ExpectationFailed, e.Message));
            }
        }
        public ActionResult KullaniciyaRolAta()
        {
            IDictionary<int, string> veriler = new Dictionary<int, string>();
            try
            {
                var kullanicilar = ctx.User.Include("Kisi").Where(x=> x.KisiId!=null).ToList();
                ViewData["Kullanicilar"] = kullanicilar;
                ViewData["Roller"] =
                    ctx.Role.ToList();          
                return View();
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public JsonResult KullaniciyaRoleAtaKaydet(int userId,int roleId)
        {
            if (!ModelState.IsValid)
            {
                return Json("Model Hatalı");
            }
            try
            {
                var user = ctx.User.Find(userId);
                var role = ctx.Role.Find(roleId);
                var userRole=new LoginEnt.UserRole()
                {
                    RoleId = roleId,
                    UserId = userId
                };
                ctx.UserRole.Add(userRole);
                ctx.SaveChanges();
                return Json("Kayıt Başarılı",JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }
        public ActionResult RolYetkiAtama()
        {
            return View();
        }
    }
}