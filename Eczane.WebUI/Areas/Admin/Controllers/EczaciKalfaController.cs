﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Web;
using System.Web.Mvc;
using Eczane.BL.Manage;
using Eczane.WebUI.Models;
using Microsoft.AspNet.Identity;

namespace Eczane.WebUI.Areas.Admin.Controllers
{
    public class EczaciKalfaController : Controller
    {
        private readonly EczaneDbContext ctx;
        public EczaciKalfaController()
        {
            ctx=new EczaneDbContext();
        }
        // GET: Admin/EczaciKalfa
        public ActionResult Index()
        {          
            var Istek = ctx.IstekBilgileri.Include("Eczaci").Include("KalfaEczaci").Include("Eczaci.User").Include("KalfaEczaci.User").Select(x=> new EczaciKalfaIstekDTO()
            {
                Id=x.Id,
                EczaciAdi = x.Eczaci.Kisi.Ad,
                EczaciSoyAdi = x.Eczaci.Kisi.SoyAd,
                Email = x.Eczaci.User.Email,
                Tel = x.Eczaci.Kisi.Telefon,
                KalfaEczaciAdi = x.KalfaEczaci.Kisi.Ad,
                KalfaEczaciSoyAdi = x.KalfaEczaci.Kisi.SoyAd,
                KalfaEmail = x.KalfaEczaci.User.Email,
                KalfaTel = x.KalfaEczaci.Kisi.Telefon,
                IstekGonderildiMi = x.IstekGonderildiMi,
                IstekGonderilmeTarihi =  x.IstekGonderilmeTarihi,
                AdminIstekOnayTarihi =  x.AdminIstekOnayTarihi
            } ).ToList();
            foreach (var item in Istek)
            {
               item.IstekGonderilmeTarihistr= String.Format("{0:d}", item.IstekGonderilmeTarihi);
                item.AdminIstekOnayTarihistr = String.Format("{0:d}", item.AdminIstekOnayTarihi);
            }
            return View(Istek);
        }
        //TODO Admin Onaylayacak Kısmı Yapılcak
        public JsonResult IstekYap(int id)
        {
            try
            {
                var result = ctx.IstekBilgileri.FirstOrDefault(x => x.Id == id);
                result.AdminIstekOnayTarihi=DateTime.Now;
                result.IstekGonderildiMi = true;
                ctx.SaveChanges();
                return Json("İşlem Başarılı",JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message); 
            }
        }
    }
}