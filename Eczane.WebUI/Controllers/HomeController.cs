﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eczane.BL.Entities;
using Microsoft.AspNet.Identity;

namespace Eczane.WebUI.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
           var username= User.Identity.GetUserName();

            ViewData["username"] = username;
            ViewData["userid"] = User.Identity.GetUserId();
            return View();
        }
    }
}