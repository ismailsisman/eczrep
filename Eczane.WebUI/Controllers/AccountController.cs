﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection.Emit;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;
using Eczane.BL.Entities;
using Eczane.BL.Manage;
using Eczane.WebUI.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin;

namespace Eczane.WebUI.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        public Oturum _ot;
        private readonly EczaneDbContext ctx;
        public AccountController()
        {
            ctx = new EczaneDbContext();
        }
        // GET: Account
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Login(RegisterDto dto, bool RememberMe = false)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Login");
            }
            LoginEnt.User user = new LoginEnt.User()
            {
                Email = dto.Email,
                UserName = dto.Email,
                Password = dto.Password
            };
            _ot = new Oturum();
            var result = await _ot.SignInManager.PasswordSignInAsync(user.UserName,
                user.Password, RememberMe, shouldLockout: false);
            //var principal = await _ot.SignInManager.CreateUserIdentityAsync(dto);
            //var currentuser = User.Identity as ClaimsIdentity;
            if (result == SignInStatus.Success)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("Email", "E-Posta hatalı olabilir");
                ModelState.AddModelError("Password", "Şifreniz  hatalı olabilir");
                HttpContext.GetOwinContext().Authentication.SignOut();
                return RedirectToAction("Login");
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Register(RegisterDto dto, int EczaciMiKalfaMi)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Login");
            }
            _ot = new Oturum();
            LoginEnt.User user = new LoginEnt.User()
            {
                Email = dto.Email,
                UserName = dto.Email,
            };
            var result = await _ot.UserManager.CreateAsync(user, dto.Password);
            if (result.Succeeded)
            {
                var userId = ctx.User.First(x => x.Email == dto.Email).Id;
                if (EczaciMiKalfaMi == 1)
                {
                    var roleId = ctx.Role.FirstOrDefault(x => x.Id == 2).Id;
                    var eczaci = new LoginEnt.Eczaci()
                    {
                        UserId = userId,
                        GIN_Numarasi = dto.GINNo
                    };
                    ctx.Eczaci.Add(eczaci);
                    ctx.SaveChanges();
                    ctx.UserRole.Add(new LoginEnt.UserRole()
                    {
                        RoleId = roleId,
                        UserId = eczaci.UserId.Value
                    });
                    ctx.SaveChanges();
                }
                else
                {
                    var roleId = ctx.Role.FirstOrDefault(x => x.Id == 3).Id;
                    var kalfa = new LoginEnt.KalfaEczaci()
                    {
                        UserId = userId
                    };
                    ctx.KalfaEczaci.Add(kalfa);
                    ctx.SaveChanges();
                    var role = new LoginEnt.UserRole()
                    {
                        RoleId = roleId,
                        UserId = kalfa.UserId.Value
                    };
                    ctx.UserRole.Add(role);
                    ctx.SaveChanges();
                }
                await _ot.SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                return Redirect("/Home/Index");
            }
            return RedirectToAction("Login", "Account");
        }
        [AllowAnonymous]
        public ActionResult LogOut()
        {
            HttpContext.GetOwinContext().Authentication.SignOut();
            return RedirectToAction("Login");
        }
    }
}