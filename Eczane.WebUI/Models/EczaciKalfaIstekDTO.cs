﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Eczane.BL.Entities;

namespace Eczane.WebUI.Models
{
    public class EczaciKalfaIstekDTO
    {
        public int? Id { get; set; }
        public string  EczaciAdi { get; set; }
        public string EczaciSoyAdi { get; set; }
        public string Tel { get; set; }
        public string Email { get; set; }
        public bool IstekGonderildiMi { get; set; } = false;
        public string  IstekGonderilmeTarihistr { get; set; }
        public DateTime? AdminIstekOnayTarihi { get; set; }
        public DateTime? IstekGonderilmeTarihi { get; set; }
        public string AdminIstekOnayTarihistr { get; set; }
        public string KalfaEczaciAdi { get; set; }
        public string KalfaEczaciSoyAdi { get; set; }
        public string KalfaTel { get; set; }
        public string KalfaEmail { get; set; }
    }
}