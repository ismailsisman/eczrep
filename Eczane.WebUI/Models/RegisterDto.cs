﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;

namespace Eczane.WebUI.Models
{
    public class RegisterDto
    {
        [Required]
        public string Email{ get; set; }
        [Required]
        public string Password { get; set; }
        public  string GINNo { get; set; }
    }
}