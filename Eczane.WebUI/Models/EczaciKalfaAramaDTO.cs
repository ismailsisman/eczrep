﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Eczane.BL.Entities;

namespace Eczane.WebUI.Models
{
    public class EczaciKalfaAramaDTO
    {
        public List<LoginEnt.Dosyalar> Dosyalar { get; set; }
        public LoginEnt.KalfaEczaci Kalfa { get; set; }
        public LoginEnt.KisiBilgiler Kisi { get; set; }
    }
}